<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>tornado.stack_context — Exception handling across asynchronous callbacks — Tornado 3.2.dev2 documentation</title>
<link href="_static/tornado.css" rel="stylesheet" type="text/css"/>
<link href="_static/pygments.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    '',
        VERSION:     '3.2.dev2',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
<script src="_static/jquery.js" type="text/javascript"></script>
<script src="_static/underscore.js" type="text/javascript"></script>
<script src="_static/doctools.js" type="text/javascript"></script>
<link href="_static/favicon.ico" rel="shortcut icon"/>
<link href="index.html" rel="top" title="Tornado 3.2.dev2 documentation"/>
<link href="utilities.html" rel="up" title="Utilities"/>
<link href="testing.html" rel="next" title="tornado.testing — Unit testing support for asynchronous code"/>
<link href="process.html" rel="prev" title="tornado.process — Utilities for multiple processes"/>
</head>
<body>
<div class="related">
<h3>Navigation</h3>
<ul>
<li class="right" style="margin-right: 10px">
<a accesskey="I" href="genindex.html" title="General Index">index</a></li>
<li class="right">
<a href="py-modindex.html" title="Python Module Index">modules</a> |</li>
<li class="right">
<a accesskey="N" href="testing.html" title="tornado.testing — Unit testing support for asynchronous code">next</a> |</li>
<li class="right">
<a accesskey="P" href="process.html" title="tornado.process — Utilities for multiple processes">previous</a> |</li>
<li><a href="index.html">Tornado 3.2.dev2 documentation</a> »</li>
<li><a href="documentation.html">Tornado Documentation</a> »</li>
<li><a accesskey="U" href="utilities.html">Utilities</a> »</li>
</ul>
</div>
<div class="document">
<div class="documentwrapper">
<div class="bodywrapper">
<div class="body">
<div class="section" id="module-tornado.stack_context">
<span id="tornado-stack-context-exception-handling-across-asynchronous-callbacks"></span><h1><a name="//apple_ref/cpp/Module/tornado.stack_context"></a><tt class="docutils literal"><span class="pre">tornado.stack_context</span></tt> — Exception handling across asynchronous callbacks<a class="headerlink" href="#module-tornado.stack_context" title="Permalink to this headline">¶</a></h1>
<p><a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a> allows applications to maintain threadlocal-like state
that follows execution as it moves to other execution contexts.</p>
<p>The motivating examples are to eliminate the need for explicit
<tt class="docutils literal"><span class="pre">async_callback</span></tt> wrappers (as in <a class="reference internal" href="web.html#tornado.web.RequestHandler" title="tornado.web.RequestHandler"><tt class="xref py py-obj docutils literal"><span class="pre">tornado.web.RequestHandler</span></tt></a>), and to
allow some additional context to be kept for logging.</p>
<p>This is slightly magic, but it’s an extension of the idea that an
exception handler is a kind of stack-local state and when that stack
is suspended and resumed in a new context that state needs to be
preserved.  <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a> shifts the burden of restoring that state
from each call site (e.g.  wrapping each <a class="reference internal" href="httpclient.html#tornado.httpclient.AsyncHTTPClient" title="tornado.httpclient.AsyncHTTPClient"><tt class="xref py py-obj docutils literal"><span class="pre">AsyncHTTPClient</span></tt></a> callback
in <tt class="docutils literal"><span class="pre">async_callback</span></tt>) to the mechanisms that transfer control from
one context to another (e.g. <a class="reference internal" href="httpclient.html#tornado.httpclient.AsyncHTTPClient" title="tornado.httpclient.AsyncHTTPClient"><tt class="xref py py-obj docutils literal"><span class="pre">AsyncHTTPClient</span></tt></a> itself, <a class="reference internal" href="ioloop.html#tornado.ioloop.IOLoop" title="tornado.ioloop.IOLoop"><tt class="xref py py-obj docutils literal"><span class="pre">IOLoop</span></tt></a>,
thread pools, etc).</p>
<p>Example usage:</p>
<div class="highlight-none"><div class="highlight"><pre>@contextlib.contextmanager
def die_on_error():
    try:
        yield
    except Exception:
        logging.error("exception in asynchronous operation",exc_info=True)
        sys.exit(1)

with StackContext(die_on_error):
    # Any exception thrown here *or in callback and its desendents*
    # will cause the process to exit instead of spinning endlessly
    # in the ioloop.
    http_client.fetch(url, callback)
ioloop.start()
</pre></div>
</div>
<p>Most applications shouln’t have to work with <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a> directly.
Here are a few rules of thumb for when it’s necessary:</p>
<ul class="simple">
<li>If you’re writing an asynchronous library that doesn’t rely on a
stack_context-aware library like <a class="reference internal" href="ioloop.html#module-tornado.ioloop" title="tornado.ioloop"><tt class="xref py py-obj docutils literal"><span class="pre">tornado.ioloop</span></tt></a> or <tt class="xref py py-obj docutils literal"><span class="pre">tornado.iostream</span></tt>
(for example, if you’re writing a thread pool), use
<a class="reference internal" href="#tornado.stack_context.wrap" title="tornado.stack_context.wrap"><tt class="xref py py-obj docutils literal"><span class="pre">stack_context.wrap()</span></tt></a> before any asynchronous operations to capture the
stack context from where the operation was started.</li>
<li>If you’re writing an asynchronous library that has some shared
resources (such as a connection pool), create those shared resources
within a <tt class="docutils literal"><span class="pre">with</span> <span class="pre">stack_context.NullContext():</span></tt> block.  This will prevent
<tt class="docutils literal"><span class="pre">StackContexts</span></tt> from leaking from one request to another.</li>
<li>If you want to write something like an exception handler that will
persist across asynchronous calls, create a new <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a> (or
<a class="reference internal" href="#tornado.stack_context.ExceptionStackContext" title="tornado.stack_context.ExceptionStackContext"><tt class="xref py py-obj docutils literal"><span class="pre">ExceptionStackContext</span></tt></a>), and make your asynchronous calls in a <tt class="docutils literal"><span class="pre">with</span></tt>
block that references your <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a>.</li>
</ul>
<dl class="class">
<dt id="tornado.stack_context.StackContext"><a name="//apple_ref/cpp/cl/tornado.stack_context.StackContext"></a>
<em class="property">class </em><tt class="descclassname">tornado.stack_context.</tt><tt class="descname">StackContext</tt><big>(</big><em>context_factory</em><big>)</big><a class="reference internal" href="_modules/tornado/stack_context.html#StackContext"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.stack_context.StackContext" title="Permalink to this definition">¶</a></dt>
<dd><p>Establishes the given context as a StackContext that will be transferred.</p>
<p>Note that the parameter is a callable that returns a context
manager, not the context itself.  That is, where for a
non-transferable context manager you would say:</p>
<div class="highlight-none"><div class="highlight"><pre>with my_context():
</pre></div>
</div>
<p>StackContext takes the function itself rather than its result:</p>
<div class="highlight-none"><div class="highlight"><pre>with StackContext(my_context):
</pre></div>
</div>
<p>The result of <tt class="docutils literal"><span class="pre">with</span> <span class="pre">StackContext()</span> <span class="pre">as</span> <span class="pre">cb:</span></tt> is a deactivation
callback.  Run this callback when the StackContext is no longer
needed to ensure that it is not propagated any further (note that
deactivating a context does not affect any instances of that
context that are currently pending).  This is an advanced feature
and not necessary in most applications.</p>
</dd></dl>
<dl class="class">
<dt id="tornado.stack_context.ExceptionStackContext"><a name="//apple_ref/cpp/cl/tornado.stack_context.ExceptionStackContext"></a>
<em class="property">class </em><tt class="descclassname">tornado.stack_context.</tt><tt class="descname">ExceptionStackContext</tt><big>(</big><em>exception_handler</em><big>)</big><a class="reference internal" href="_modules/tornado/stack_context.html#ExceptionStackContext"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.stack_context.ExceptionStackContext" title="Permalink to this definition">¶</a></dt>
<dd><p>Specialization of StackContext for exception handling.</p>
<p>The supplied <tt class="docutils literal"><span class="pre">exception_handler</span></tt> function will be called in the
event of an uncaught exception in this context.  The semantics are
similar to a try/finally clause, and intended use cases are to log
an error, close a socket, or similar cleanup actions.  The
<tt class="docutils literal"><span class="pre">exc_info</span></tt> triple <tt class="docutils literal"><span class="pre">(type,</span> <span class="pre">value,</span> <span class="pre">traceback)</span></tt> will be passed to the
exception_handler function.</p>
<p>If the exception handler returns true, the exception will be
consumed and will not be propagated to other exception handlers.</p>
</dd></dl>
<dl class="class">
<dt id="tornado.stack_context.NullContext"><a name="//apple_ref/cpp/cl/tornado.stack_context.NullContext"></a>
<em class="property">class </em><tt class="descclassname">tornado.stack_context.</tt><tt class="descname">NullContext</tt><a class="reference internal" href="_modules/tornado/stack_context.html#NullContext"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.stack_context.NullContext" title="Permalink to this definition">¶</a></dt>
<dd><p>Resets the <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a>.</p>
<p>Useful when creating a shared resource on demand (e.g. an
<a class="reference internal" href="httpclient.html#tornado.httpclient.AsyncHTTPClient" title="tornado.httpclient.AsyncHTTPClient"><tt class="xref py py-obj docutils literal"><span class="pre">AsyncHTTPClient</span></tt></a>) where the stack that caused the creating is
not relevant to future operations.</p>
</dd></dl>
<dl class="function">
<dt id="tornado.stack_context.wrap"><a name="//apple_ref/cpp/func/tornado.stack_context.wrap"></a>
<tt class="descclassname">tornado.stack_context.</tt><tt class="descname">wrap</tt><big>(</big><em>fn</em><big>)</big><a class="reference internal" href="_modules/tornado/stack_context.html#wrap"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.stack_context.wrap" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns a callable object that will restore the current <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a>
when executed.</p>
<p>Use this whenever saving a callback to be executed later in a
different execution context (either in a different thread or
asynchronously in the same thread).</p>
</dd></dl>
<dl class="function">
<dt id="tornado.stack_context.run_with_stack_context"><a name="//apple_ref/cpp/func/tornado.stack_context.run_with_stack_context"></a>
<tt class="descclassname">tornado.stack_context.</tt><tt class="descname">run_with_stack_context</tt><big>(</big><em>context</em>, <em>func</em><big>)</big><a class="reference internal" href="_modules/tornado/stack_context.html#run_with_stack_context"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.stack_context.run_with_stack_context" title="Permalink to this definition">¶</a></dt>
<dd><p>Run a coroutine <tt class="docutils literal"><span class="pre">func</span></tt> in the given <a class="reference internal" href="#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a>.</p>
<p>It is not safe to have a <tt class="docutils literal"><span class="pre">yield</span></tt> statement within a <tt class="docutils literal"><span class="pre">with</span> <span class="pre">StackContext</span></tt>
block, so it is difficult to use stack context with <a class="reference internal" href="gen.html#tornado.gen.coroutine" title="tornado.gen.coroutine"><tt class="xref py py-obj docutils literal"><span class="pre">gen.coroutine</span></tt></a>.
This helper function runs the function in the correct context while
keeping the <tt class="docutils literal"><span class="pre">yield</span></tt> and <tt class="docutils literal"><span class="pre">with</span></tt> statements syntactically separate.</p>
<p>Example:</p>
<div class="highlight-none"><div class="highlight"><pre>@gen.coroutine
def incorrect():
    with StackContext(ctx):
        # ERROR: this will raise StackContextInconsistentError
        yield other_coroutine()

@gen.coroutine
def correct():
    yield run_with_stack_context(StackContext(ctx), other_coroutine)
</pre></div>
</div>
<p class="versionadded">
<span class="versionmodified">New in version 3.1.</span></p>
</dd></dl>
</div>
</div>
</div>
</div>
<div class="sphinxsidebar">
<div class="sphinxsidebarwrapper">
<h4>Previous topic</h4>
<p class="topless"><a href="process.html" title="previous chapter"><tt class="docutils literal"><span class="pre">tornado.process</span></tt> — Utilities for multiple processes</a></p>
<h4>Next topic</h4>
<p class="topless"><a href="testing.html" title="next chapter"><tt class="docutils literal"><span class="pre">tornado.testing</span></tt> — Unit testing support for asynchronous code</a></p>
<h3>This Page</h3>
<ul class="this-page-menu">
<li><a href="_sources/stack_context.txt" rel="nofollow">Show Source</a></li>
</ul>
<div id="searchbox" style="display: none">
<h3>Quick search</h3>
<form action="search.html" class="search" method="get">
<input name="q" type="text"/>
<input type="submit" value="Go"/>
<input name="check_keywords" type="hidden" value="yes"/>
<input name="area" type="hidden" value="default"/>
</form>
<p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
</div>
</div>
<div class="clearer"></div>
</div>
<div class="related">
<h3>Navigation</h3>
<ul>
<li class="right" style="margin-right: 10px">
<a href="genindex.html" title="General Index">index</a></li>
<li class="right">
<a href="py-modindex.html" title="Python Module Index">modules</a> |</li>
<li class="right">
<a href="testing.html" title="tornado.testing — Unit testing support for asynchronous code">next</a> |</li>
<li class="right">
<a href="process.html" title="tornado.process — Utilities for multiple processes">previous</a> |</li>
<li><a href="index.html">Tornado 3.2.dev2 documentation</a> »</li>
<li><a href="documentation.html">Tornado Documentation</a> »</li>
<li><a href="utilities.html">Utilities</a> »</li>
</ul>
</div>
<div class="footer">
        © Copyright 2011, Facebook.
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 1.1.3.
    </div>
</body>
</html>