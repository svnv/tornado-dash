<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<title>tornado.gen — Simplify asynchronous code — Tornado 3.2.dev2 documentation</title>
<link href="_static/tornado.css" rel="stylesheet" type="text/css"/>
<link href="_static/pygments.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    '',
        VERSION:     '3.2.dev2',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
<script src="_static/jquery.js" type="text/javascript"></script>
<script src="_static/underscore.js" type="text/javascript"></script>
<script src="_static/doctools.js" type="text/javascript"></script>
<link href="_static/favicon.ico" rel="shortcut icon"/>
<link href="index.html" rel="top" title="Tornado 3.2.dev2 documentation"/>
<link href="networking.html" rel="up" title="Asynchronous networking"/>
<link href="ioloop.html" rel="next" title="tornado.ioloop — Main event loop"/>
<link href="networking.html" rel="prev" title="Asynchronous networking"/>
</head>
<body>
<div class="related">
<h3>Navigation</h3>
<ul>
<li class="right" style="margin-right: 10px">
<a accesskey="I" href="genindex.html" title="General Index">index</a></li>
<li class="right">
<a href="py-modindex.html" title="Python Module Index">modules</a> |</li>
<li class="right">
<a accesskey="N" href="ioloop.html" title="tornado.ioloop — Main event loop">next</a> |</li>
<li class="right">
<a accesskey="P" href="networking.html" title="Asynchronous networking">previous</a> |</li>
<li><a href="index.html">Tornado 3.2.dev2 documentation</a> »</li>
<li><a href="documentation.html">Tornado Documentation</a> »</li>
<li><a accesskey="U" href="networking.html">Asynchronous networking</a> »</li>
</ul>
</div>
<div class="document">
<div class="documentwrapper">
<div class="bodywrapper">
<div class="body">
<div class="section" id="module-tornado.gen">
<span id="tornado-gen-simplify-asynchronous-code"></span><h1><a name="//apple_ref/cpp/Module/tornado.gen"></a><tt class="docutils literal"><span class="pre">tornado.gen</span></tt> — Simplify asynchronous code<a class="headerlink" href="#module-tornado.gen" title="Permalink to this headline">¶</a></h1>
<p><tt class="docutils literal"><span class="pre">tornado.gen</span></tt> is a generator-based interface to make it easier to
work in an asynchronous environment.  Code using the <tt class="docutils literal"><span class="pre">gen</span></tt> module
is technically asynchronous, but it is written as a single generator
instead of a collection of separate functions.</p>
<p>For example, the following asynchronous handler:</p>
<div class="highlight-none"><div class="highlight"><pre>class AsyncHandler(RequestHandler):
    @asynchronous
    def get(self):
        http_client = AsyncHTTPClient()
        http_client.fetch("http://example.com",
                          callback=self.on_fetch)

    def on_fetch(self, response):
        do_something_with_response(response)
        self.render("template.html")
</pre></div>
</div>
<p>could be written with <tt class="docutils literal"><span class="pre">gen</span></tt> as:</p>
<div class="highlight-none"><div class="highlight"><pre>class GenAsyncHandler(RequestHandler):
    @gen.coroutine
    def get(self):
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch("http://example.com")
        do_something_with_response(response)
        self.render("template.html")
</pre></div>
</div>
<p>Most asynchronous functions in Tornado return a <a class="reference internal" href="concurrent.html#tornado.concurrent.Future" title="tornado.concurrent.Future"><tt class="xref py py-obj docutils literal"><span class="pre">Future</span></tt></a>;
yielding this object returns its <a class="reference internal" href="concurrent.html#tornado.concurrent.Future.result" title="tornado.concurrent.Future.result"><tt class="xref py py-obj docutils literal"><span class="pre">result</span></tt></a>.</p>
<p>For functions that do not return <tt class="docutils literal"><span class="pre">Futures</span></tt>, <a class="reference internal" href="#tornado.gen.Task" title="tornado.gen.Task"><tt class="xref py py-obj docutils literal"><span class="pre">Task</span></tt></a> works with any
function that takes a <tt class="docutils literal"><span class="pre">callback</span></tt> keyword argument (most Tornado functions
can be used in either style, although the <tt class="docutils literal"><span class="pre">Future</span></tt> style is preferred
since it is both shorter and provides better exception handling):</p>
<div class="highlight-none"><div class="highlight"><pre>@gen.coroutine
def get(self):
    yield gen.Task(AsyncHTTPClient().fetch, "http://example.com")
</pre></div>
</div>
<p>You can also yield a list or dict of <tt class="docutils literal"><span class="pre">Futures</span></tt> and/or <tt class="docutils literal"><span class="pre">Tasks</span></tt>, which will be
started at the same time and run in parallel; a list or dict of results will
be returned when they are all finished:</p>
<div class="highlight-none"><div class="highlight"><pre>@gen.coroutine
def get(self):
    http_client = AsyncHTTPClient()
    response1, response2 = yield [http_client.fetch(url1),
                                  http_client.fetch(url2)]
    response_dict = yield dict(response3=http_client.fetch(url3),
                               response4=http_client.fetch(url4))
    response3 = response_dict['response3']
    response4 = response_dict['response4']
</pre></div>
</div>
<p class="versionchanged">
<span class="versionmodified">Changed in version 3.2: </span>Dict support added.</p>
<p>For more complicated interfaces, <a class="reference internal" href="#tornado.gen.Task" title="tornado.gen.Task"><tt class="xref py py-obj docutils literal"><span class="pre">Task</span></tt></a> can be split into two parts:
<a class="reference internal" href="#tornado.gen.Callback" title="tornado.gen.Callback"><tt class="xref py py-obj docutils literal"><span class="pre">Callback</span></tt></a> and <a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a>:</p>
<div class="highlight-none"><div class="highlight"><pre>class GenAsyncHandler2(RequestHandler):
    @asynchronous
    @gen.coroutine
    def get(self):
        http_client = AsyncHTTPClient()
        http_client.fetch("http://example.com",
                          callback=(yield gen.Callback("key"))
        response = yield gen.Wait("key")
        do_something_with_response(response)
        self.render("template.html")
</pre></div>
</div>
<p>The <tt class="docutils literal"><span class="pre">key</span></tt> argument to <a class="reference internal" href="#tornado.gen.Callback" title="tornado.gen.Callback"><tt class="xref py py-obj docutils literal"><span class="pre">Callback</span></tt></a> and <a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a> allows for multiple
asynchronous operations to be started at different times and proceed
in parallel: yield several callbacks with different keys, then wait
for them once all the async operations have started.</p>
<p>The result of a <a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a> or <a class="reference internal" href="#tornado.gen.Task" title="tornado.gen.Task"><tt class="xref py py-obj docutils literal"><span class="pre">Task</span></tt></a> yield expression depends on how the callback
was run.  If it was called with no arguments, the result is <tt class="docutils literal"><span class="pre">None</span></tt>.  If
it was called with one argument, the result is that argument.  If it was
called with more than one argument or any keyword arguments, the result
is an <a class="reference internal" href="#tornado.gen.Arguments" title="tornado.gen.Arguments"><tt class="xref py py-obj docutils literal"><span class="pre">Arguments</span></tt></a> object, which is a named tuple <tt class="docutils literal"><span class="pre">(args,</span> <span class="pre">kwargs)</span></tt>.</p>
<div class="section" id="decorators">
<h2>Decorators<a class="headerlink" href="#decorators" title="Permalink to this headline">¶</a></h2>
<dl class="function">
<dt id="tornado.gen.coroutine"><a name="//apple_ref/cpp/func/tornado.gen.coroutine"></a>
<tt class="descclassname">tornado.gen.</tt><tt class="descname">coroutine</tt><big>(</big><em>func</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#coroutine"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.coroutine" title="Permalink to this definition">¶</a></dt>
<dd><p>Decorator for asynchronous generators.</p>
<p>Any generator that yields objects from this module must be wrapped
in either this decorator or <a class="reference internal" href="#tornado.gen.engine" title="tornado.gen.engine"><tt class="xref py py-obj docutils literal"><span class="pre">engine</span></tt></a>.</p>
<p>Coroutines may “return” by raising the special exception
<a class="reference internal" href="#tornado.gen.Return" title="tornado.gen.Return"><tt class="xref py py-obj docutils literal"><span class="pre">Return(value)</span></tt></a>.  In Python 3.3+, it is also possible for
the function to simply use the <tt class="docutils literal"><span class="pre">return</span> <span class="pre">value</span></tt> statement (prior to
Python 3.3 generators were not allowed to also return values).
In all versions of Python a coroutine that simply wishes to exit
early may use the <tt class="docutils literal"><span class="pre">return</span></tt> statement without a value.</p>
<p>Functions with this decorator return a <a class="reference internal" href="concurrent.html#tornado.concurrent.Future" title="tornado.concurrent.Future"><tt class="xref py py-obj docutils literal"><span class="pre">Future</span></tt></a>.  Additionally,
they may be called with a <tt class="docutils literal"><span class="pre">callback</span></tt> keyword argument, which
will be invoked with the future’s result when it resolves.  If the
coroutine fails, the callback will not be run and an exception
will be raised into the surrounding <a class="reference internal" href="stack_context.html#tornado.stack_context.StackContext" title="tornado.stack_context.StackContext"><tt class="xref py py-obj docutils literal"><span class="pre">StackContext</span></tt></a>.  The
<tt class="docutils literal"><span class="pre">callback</span></tt> argument is not visible inside the decorated
function; it is handled by the decorator itself.</p>
<p>From the caller’s perspective, <tt class="docutils literal"><span class="pre">@gen.coroutine</span></tt> is similar to
the combination of <tt class="docutils literal"><span class="pre">@return_future</span></tt> and <tt class="docutils literal"><span class="pre">@gen.engine</span></tt>.</p>
</dd></dl>
<dl class="function">
<dt id="tornado.gen.engine"><a name="//apple_ref/cpp/func/tornado.gen.engine"></a>
<tt class="descclassname">tornado.gen.</tt><tt class="descname">engine</tt><big>(</big><em>func</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#engine"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.engine" title="Permalink to this definition">¶</a></dt>
<dd><p>Callback-oriented decorator for asynchronous generators.</p>
<p>This is an older interface; for new code that does not need to be
compatible with versions of Tornado older than 3.0 the
<a class="reference internal" href="#tornado.gen.coroutine" title="tornado.gen.coroutine"><tt class="xref py py-obj docutils literal"><span class="pre">coroutine</span></tt></a> decorator is recommended instead.</p>
<p>This decorator is similar to <a class="reference internal" href="#tornado.gen.coroutine" title="tornado.gen.coroutine"><tt class="xref py py-obj docutils literal"><span class="pre">coroutine</span></tt></a>, except it does not
return a <a class="reference internal" href="concurrent.html#tornado.concurrent.Future" title="tornado.concurrent.Future"><tt class="xref py py-obj docutils literal"><span class="pre">Future</span></tt></a> and the <tt class="docutils literal"><span class="pre">callback</span></tt> argument is not treated
specially.</p>
<p>In most cases, functions decorated with <a class="reference internal" href="#tornado.gen.engine" title="tornado.gen.engine"><tt class="xref py py-obj docutils literal"><span class="pre">engine</span></tt></a> should take
a <tt class="docutils literal"><span class="pre">callback</span></tt> argument and invoke it with their result when
they are finished.  One notable exception is the
<a class="reference internal" href="web.html#tornado.web.RequestHandler" title="tornado.web.RequestHandler"><tt class="xref py py-obj docutils literal"><span class="pre">RequestHandler</span></tt></a> <a class="reference internal" href="web.html#verbs"><em>HTTP verb methods</em></a>,
which use <tt class="docutils literal"><span class="pre">self.finish()</span></tt> in place of a callback argument.</p>
</dd></dl>
</div>
<div class="section" id="yield-points">
<h2>Yield points<a class="headerlink" href="#yield-points" title="Permalink to this headline">¶</a></h2>
<p>Instances of the following classes may be used in yield expressions
in the generator.  <a class="reference internal" href="concurrent.html#tornado.concurrent.Future" title="tornado.concurrent.Future"><tt class="xref py py-obj docutils literal"><span class="pre">Futures</span></tt></a> may be yielded as well;
their result method will be called automatically when they are
ready.  Additionally, lists of any combination of these objects may
be yielded; the result is a list of the results of each yield point
in the same order. Yielding dicts with these objects in values will
return dict with results at the same keys.</p>
<dl class="class">
<dt id="tornado.gen.Task"><a name="//apple_ref/cpp/cl/tornado.gen.Task"></a>
<em class="property">class </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">Task</tt><big>(</big><em>func</em>, <em>*args</em>, <em>**kwargs</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#Task"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.Task" title="Permalink to this definition">¶</a></dt>
<dd><p>Runs a single asynchronous operation.</p>
<p>Takes a function (and optional additional arguments) and runs it with
those arguments plus a <tt class="docutils literal"><span class="pre">callback</span></tt> keyword argument.  The argument passed
to the callback is returned as the result of the yield expression.</p>
<p>A <a class="reference internal" href="#tornado.gen.Task" title="tornado.gen.Task"><tt class="xref py py-obj docutils literal"><span class="pre">Task</span></tt></a> is equivalent to a <a class="reference internal" href="#tornado.gen.Callback" title="tornado.gen.Callback"><tt class="xref py py-obj docutils literal"><span class="pre">Callback</span></tt></a>/<a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a> pair (with a unique
key generated automatically):</p>
<div class="highlight-none"><div class="highlight"><pre>result = yield gen.Task(func, args)

func(args, callback=(yield gen.Callback(key)))
result = yield gen.Wait(key)
</pre></div>
</div>
</dd></dl>
<dl class="class">
<dt id="tornado.gen.Callback"><a name="//apple_ref/cpp/cl/tornado.gen.Callback"></a>
<em class="property">class </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">Callback</tt><big>(</big><em>key</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#Callback"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.Callback" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns a callable object that will allow a matching <a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a> to proceed.</p>
<p>The key may be any value suitable for use as a dictionary key, and is
used to match <tt class="docutils literal"><span class="pre">Callbacks</span></tt> to their corresponding <tt class="docutils literal"><span class="pre">Waits</span></tt>.  The key
must be unique among outstanding callbacks within a single run of the
generator function, but may be reused across different runs of the same
function (so constants generally work fine).</p>
<p>The callback may be called with zero or one arguments; if an argument
is given it will be returned by <a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a>.</p>
</dd></dl>
<dl class="class">
<dt id="tornado.gen.Wait"><a name="//apple_ref/cpp/cl/tornado.gen.Wait"></a>
<em class="property">class </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">Wait</tt><big>(</big><em>key</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#Wait"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.Wait" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns the argument passed to the result of a previous <a class="reference internal" href="#tornado.gen.Callback" title="tornado.gen.Callback"><tt class="xref py py-obj docutils literal"><span class="pre">Callback</span></tt></a>.</p>
</dd></dl>
<dl class="class">
<dt id="tornado.gen.WaitAll"><a name="//apple_ref/cpp/cl/tornado.gen.WaitAll"></a>
<em class="property">class </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">WaitAll</tt><big>(</big><em>keys</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#WaitAll"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.WaitAll" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns the results of multiple previous <a class="reference internal" href="#tornado.gen.Callback" title="tornado.gen.Callback"><tt class="xref py py-obj docutils literal"><span class="pre">Callbacks</span></tt></a>.</p>
<p>The argument is a sequence of <a class="reference internal" href="#tornado.gen.Callback" title="tornado.gen.Callback"><tt class="xref py py-obj docutils literal"><span class="pre">Callback</span></tt></a> keys, and the result is
a list of results in the same order.</p>
<p><a class="reference internal" href="#tornado.gen.WaitAll" title="tornado.gen.WaitAll"><tt class="xref py py-obj docutils literal"><span class="pre">WaitAll</span></tt></a> is equivalent to yielding a list of <a class="reference internal" href="#tornado.gen.Wait" title="tornado.gen.Wait"><tt class="xref py py-obj docutils literal"><span class="pre">Wait</span></tt></a> objects.</p>
</dd></dl>
<dl class="class">
<dt id="tornado.gen.YieldPoint"><a name="//apple_ref/cpp/cl/tornado.gen.YieldPoint"></a>
<em class="property">class </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">YieldPoint</tt><a class="reference internal" href="_modules/tornado/gen.html#YieldPoint"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.YieldPoint" title="Permalink to this definition">¶</a></dt>
<dd><p>Base class for objects that may be yielded from the generator.</p>
<p>Applications do not normally need to use this class, but it may be
subclassed to provide additional yielding behavior.</p>
<dl class="method">
<dt id="tornado.gen.YieldPoint.start"><a name="//apple_ref/cpp/clm/tornado.gen.YieldPoint.start"></a>
<tt class="descname">start</tt><big>(</big><em>runner</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#YieldPoint.start"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.YieldPoint.start" title="Permalink to this definition">¶</a></dt>
<dd><p>Called by the runner after the generator has yielded.</p>
<p>No other methods will be called on this object before <tt class="docutils literal"><span class="pre">start</span></tt>.</p>
</dd></dl>
<dl class="method">
<dt id="tornado.gen.YieldPoint.is_ready"><a name="//apple_ref/cpp/clm/tornado.gen.YieldPoint.is_ready"></a>
<tt class="descname">is_ready</tt><big>(</big><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#YieldPoint.is_ready"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.YieldPoint.is_ready" title="Permalink to this definition">¶</a></dt>
<dd><p>Called by the runner to determine whether to resume the generator.</p>
<p>Returns a boolean; may be called more than once.</p>
</dd></dl>
<dl class="method">
<dt id="tornado.gen.YieldPoint.get_result"><a name="//apple_ref/cpp/clm/tornado.gen.YieldPoint.get_result"></a>
<tt class="descname">get_result</tt><big>(</big><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#YieldPoint.get_result"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.YieldPoint.get_result" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns the value to use as the result of the yield expression.</p>
<p>This method will only be called once, and only after <a class="reference internal" href="#tornado.gen.YieldPoint.is_ready" title="tornado.gen.YieldPoint.is_ready"><tt class="xref py py-obj docutils literal"><span class="pre">is_ready</span></tt></a>
has returned true.</p>
</dd></dl>
</dd></dl>
</div>
<div class="section" id="other-classes">
<h2>Other classes<a class="headerlink" href="#other-classes" title="Permalink to this headline">¶</a></h2>
<dl class="exception">
<dt id="tornado.gen.Return">
<em class="property">exception </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">Return</tt><big>(</big><em>value=None</em><big>)</big><a class="reference internal" href="_modules/tornado/gen.html#Return"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#tornado.gen.Return" title="Permalink to this definition">¶</a></dt>
<dd><p>Special exception to return a value from a <a class="reference internal" href="#tornado.gen.coroutine" title="tornado.gen.coroutine"><tt class="xref py py-obj docutils literal"><span class="pre">coroutine</span></tt></a>.</p>
<p>If this exception is raised, its value argument is used as the
result of the coroutine:</p>
<div class="highlight-none"><div class="highlight"><pre>@gen.coroutine
def fetch_json(url):
    response = yield AsyncHTTPClient().fetch(url)
    raise gen.Return(json_decode(response.body))
</pre></div>
</div>
<p>In Python 3.3, this exception is no longer necessary: the <tt class="docutils literal"><span class="pre">return</span></tt>
statement can be used directly to return a value (previously
<tt class="docutils literal"><span class="pre">yield</span></tt> and <tt class="docutils literal"><span class="pre">return</span></tt> with a value could not be combined in the
same function).</p>
<p>By analogy with the return statement, the value argument is optional,
but it is never necessary to <tt class="docutils literal"><span class="pre">raise</span> <span class="pre">gen.Return()</span></tt>.  The <tt class="docutils literal"><span class="pre">return</span></tt>
statement can be used with no arguments instead.</p>
</dd></dl>
<dl class="class">
<dt id="tornado.gen.Arguments"><a name="//apple_ref/cpp/cl/tornado.gen.Arguments"></a>
<em class="property">class </em><tt class="descclassname">tornado.gen.</tt><tt class="descname">Arguments</tt><a class="headerlink" href="#tornado.gen.Arguments" title="Permalink to this definition">¶</a></dt>
<dd><p>The result of a yield expression whose callback had more than one
argument (or keyword arguments).</p>
<p>The <a class="reference internal" href="#tornado.gen.Arguments" title="tornado.gen.Arguments"><tt class="xref py py-obj docutils literal"><span class="pre">Arguments</span></tt></a> object is a <a class="reference external" href="http://python.readthedocs.org/en/latest/library/collections.html#collections.namedtuple" title="(in Python v3.4)"><tt class="xref py py-obj docutils literal"><span class="pre">collections.namedtuple</span></tt></a> and can be
used either as a tuple <tt class="docutils literal"><span class="pre">(args,</span> <span class="pre">kwargs)</span></tt> or an object with attributes
<tt class="docutils literal"><span class="pre">args</span></tt> and <tt class="docutils literal"><span class="pre">kwargs</span></tt>.</p>
</dd></dl>
</div>
</div>
</div>
</div>
</div>
<div class="sphinxsidebar">
<div class="sphinxsidebarwrapper">
<h3><a href="index.html">Table Of Contents</a></h3>
<ul>
<li><a class="reference internal" href="#"><tt class="docutils literal"><span class="pre">tornado.gen</span></tt> — Simplify asynchronous code</a><ul>
<li><a class="reference internal" href="#decorators">Decorators</a></li>
<li><a class="reference internal" href="#yield-points">Yield points</a></li>
<li><a class="reference internal" href="#other-classes">Other classes</a></li>
</ul>
</li>
</ul>
<h4>Previous topic</h4>
<p class="topless"><a href="networking.html" title="previous chapter">Asynchronous networking</a></p>
<h4>Next topic</h4>
<p class="topless"><a href="ioloop.html" title="next chapter"><tt class="docutils literal"><span class="pre">tornado.ioloop</span></tt> — Main event loop</a></p>
<h3>This Page</h3>
<ul class="this-page-menu">
<li><a href="_sources/gen.txt" rel="nofollow">Show Source</a></li>
</ul>
<div id="searchbox" style="display: none">
<h3>Quick search</h3>
<form action="search.html" class="search" method="get">
<input name="q" type="text"/>
<input type="submit" value="Go"/>
<input name="check_keywords" type="hidden" value="yes"/>
<input name="area" type="hidden" value="default"/>
</form>
<p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
</div>
</div>
<div class="clearer"></div>
</div>
<div class="related">
<h3>Navigation</h3>
<ul>
<li class="right" style="margin-right: 10px">
<a href="genindex.html" title="General Index">index</a></li>
<li class="right">
<a href="py-modindex.html" title="Python Module Index">modules</a> |</li>
<li class="right">
<a href="ioloop.html" title="tornado.ioloop — Main event loop">next</a> |</li>
<li class="right">
<a href="networking.html" title="Asynchronous networking">previous</a> |</li>
<li><a href="index.html">Tornado 3.2.dev2 documentation</a> »</li>
<li><a href="documentation.html">Tornado Documentation</a> »</li>
<li><a href="networking.html">Asynchronous networking</a> »</li>
</ul>
</div>
<div class="footer">
        © Copyright 2011, Facebook.
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 1.1.3.
    </div>
</body>
</html>